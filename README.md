Polish an initial viral (small!) reference by iteratively mapping
variant calling and integration. For high coverage samples
downsampling is a good idea

Requires bwa, samtools, lofreq, bcfools and tabix
